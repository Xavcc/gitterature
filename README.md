# Gittérature

Gittérature – une initiative qui vise à documenter, répertorier, voire théoriser la littérature [Git](https://git-scm.com/).

## Répertoire

_Encore très préliminaire, cette liste pourra être subdivisée selon les différentes catégories que le collaborateur trouvera pertinentes._

- [Git Book](http://brendandawes.com/blog/gitbook) – Billet de blogue qui présente le projet de Brendan Dawes.
- [Cheminement textuel](https://www.quaternum.net/2019/12/09/cheminement-textuel/) – Un texte de création issu de l’enchaînement de messages Git, par Antoine Fauchié.
- [Piece 01](https://scolaire.loupbrun.ca/piece01/) – Une «pièce de théâtre», dont les dialogues correspondent aux messages de commits des différents personnages (avec signature GPG), par Louis-Olivier Brassard.
- [Abrüpt](https://abrupt.ch) – Une maison de publication indépendante qui propose des publications numériques, imprimées et DIY.
- [Ecridil](https://framagit.org/ecrinum/ecridil-booksprint) Booksprint avec usage de gitlab et mise en place de chaine éditoriale md/pandoc/html/css-print.

## Licence

[CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/) – Domaine public.
